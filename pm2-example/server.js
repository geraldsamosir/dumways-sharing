require('dotenv');
const express = require('express');
const app = express();
const { env } = process;


app.get('/', (req, res) => {
  res.json({
    instance: env.pm_id
  });
});


app.listen(3000, ()=> console.log('yeah run in 3000'));